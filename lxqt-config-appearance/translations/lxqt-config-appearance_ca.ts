<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca">
<context>
    <name>ColorLabel</name>
    <message>
        <location filename="../colorLabel.cpp" line="37"/>
        <source>Click to change color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../colorLabel.cpp" line="61"/>
        <source>Select Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigOtherToolKits</name>
    <message>
        <location filename="../configothertoolkits.cpp" line="170"/>
        <source>GTK themes</source>
        <translation>Temes de GTK</translation>
    </message>
    <message>
        <location filename="../configothertoolkits.cpp" line="171"/>
        <source>&lt;p&gt;&apos;%1&apos; has been overwritten.&lt;/p&gt;&lt;p&gt;You can find a copy of your old settings in &apos;%2&apos;&lt;/p&gt;</source>
        <translation>&lt;p&gt;«%1» ha estat sobreescrit.&lt;/p&gt;&lt;p&gt;Podeu trobar una còpia dels vostres ajusts antics a «%2»&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../configothertoolkits.cpp" line="235"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configothertoolkits.cpp" line="235"/>
        <source>Error: gsettings cannot be run</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FontsConfig</name>
    <message>
        <location filename="../fontsconfig.ui" line="23"/>
        <source>Font</source>
        <translation>Lletra</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="30"/>
        <source>Default font for user interface</source>
        <translation>Lletra predeterminada per a la interfície d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="36"/>
        <source>Font name:</source>
        <translation>Nom de la lletra:</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="46"/>
        <source>Style:</source>
        <translation>Estil:</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="53"/>
        <source>Point size:</source>
        <translation>Mida del punt:</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="68"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="73"/>
        <source>Bold</source>
        <translation>Negreta</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="78"/>
        <source>Italic</source>
        <translation>Cursiva</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="83"/>
        <source>Bold Italic</source>
        <translation>Cursiva en negreta</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="94"/>
        <source>The following settings only affect newly started applications</source>
        <translation>Els següents ajusts únicament tindran efecte amb les aplicacions iniciades a partir d&apos;ara</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="100"/>
        <source>Use antialias fonts</source>
        <translation>Utilitza les lletres amb antialiàsing</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="107"/>
        <source>Font hinting style:</source>
        <translation>Estil del contorn de la lletra:</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="118"/>
        <location filename="../fontsconfig.ui" line="183"/>
        <source>None</source>
        <translation>Sense</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="123"/>
        <source>Slight</source>
        <translation>Lleu</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="128"/>
        <source>Medium</source>
        <translation>Mitjà</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="133"/>
        <source>Full</source>
        <translation>Complet</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="141"/>
        <source>Font hinting</source>
        <translation>Contorn de la lletra</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="148"/>
        <source>Resolution (DPI):</source>
        <translation>Resolució (PPP):</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="155"/>
        <source>Autohint</source>
        <translation>Contorn automàtic</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="172"/>
        <source>Subpixel antialiasing:</source>
        <translation>Antialiàsing dels subpíxels:</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="188"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="193"/>
        <source>BGR</source>
        <translation>BGR</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="198"/>
        <source>VRGB</source>
        <translation>VRGB</translation>
    </message>
    <message>
        <location filename="../fontsconfig.ui" line="203"/>
        <source>VBGR</source>
        <translation>VBGR</translation>
    </message>
</context>
<context>
    <name>IconThemeConfig</name>
    <message>
        <location filename="../iconthemeconfig.ui" line="14"/>
        <source>LXQt Appearance Configuration</source>
        <translation>Configuració de l&apos;aparença de LXQt</translation>
    </message>
    <message>
        <location filename="../iconthemeconfig.ui" line="26"/>
        <source>Icons Theme</source>
        <translation>Tema de les icones</translation>
    </message>
    <message>
        <location filename="../iconthemeconfig.ui" line="87"/>
        <source>The KDE extension of XDG icon themes -&gt; FollowsColorScheme</source>
        <translation>L&apos;extensió de KDE dels temes d&apos;icones XDG -&gt; FollowsColorScheme</translation>
    </message>
    <message>
        <location filename="../iconthemeconfig.ui" line="90"/>
        <source>Colorize icons based on widget style (palette)</source>
        <translation>Acoloreix les icones en funció de l&apos;estil de l&apos;estri (paleta)</translation>
    </message>
</context>
<context>
    <name>LXQtThemeConfig</name>
    <message>
        <location filename="../lxqtthemeconfig.ui" line="23"/>
        <source>LXQt Theme</source>
        <translation>Tema de LXQt</translation>
    </message>
    <message>
        <location filename="../lxqtthemeconfig.ui" line="57"/>
        <source>Override user-defined wallpaper</source>
        <translation>Anul·la el fons de pantalla definit per l&apos;usuari</translation>
    </message>
</context>
<context>
    <name>PalettesDialog</name>
    <message>
        <location filename="../palettes.ui" line="14"/>
        <location filename="../palettes.ui" line="20"/>
        <source>Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../palettes.ui" line="33"/>
        <source>Filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../palettes.ui" line="43"/>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="61"/>
        <source>LXQt Appearance Configuration</source>
        <translation>Configuració de l&apos;aparença de LXQt</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>Widget Style</source>
        <translation>Estil dels estris</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="82"/>
        <source>Icons Theme</source>
        <translation>Tema de les icones</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="91"/>
        <source>LXQt Theme</source>
        <translation>Tema de LXQt</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="99"/>
        <source>Font</source>
        <translation>Lletra</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="109"/>
        <source>Cursor</source>
        <translation>Cursor</translation>
    </message>
</context>
<context>
    <name>StyleConfig</name>
    <message>
        <location filename="../styleconfig.ui" line="33"/>
        <source>Widget Style</source>
        <translation>Estil dels estris</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="63"/>
        <source>Qt Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="77"/>
        <source>Some Qt styles may ignore these colors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="102"/>
        <source>Window:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="112"/>
        <source>View:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="122"/>
        <source>Selection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="132"/>
        <source>Link:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="152"/>
        <source>Window Text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="162"/>
        <source>View Text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="172"/>
        <source>Selected Text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="182"/>
        <source>Visited Link:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="214"/>
        <source>&amp;Save Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="221"/>
        <source>&amp;Load Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="241"/>
        <source>&amp;Default Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="268"/>
        <source>Set GTK themes (GTK configuration files will be overwritten!)</source>
        <translation>Estableix els temes de GTK (es sobreescriuran els fitxers de configuració de GTK!)</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="288"/>
        <source>GTK 3 Theme</source>
        <translation>Tema de GTK 3</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="298"/>
        <source>GTK 2 Theme</source>
        <translation>Tema de GTK 2</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="308"/>
        <source>To attempt uniform theming, either select similar style/theme (if available) across all lists, or select &apos;gtk2&apos; Qt style (if available) to mimic GTK themes.

Make sure &apos;xsettingsd&apos; is installed to help GTK applications apply themes on the fly.</source>
        <translation>A fi de tenir un tema uniforme, seleccioneu un estil o tema similar del llistat, o seleccioneu l&apos;estil «gtk2» de Qt per imitar els temes de GTK.

Assegureu-vos que «xsettingsd» està instal·lat perquè ajudi a les aplicacions GTK aplicar temes sobre la marxa.</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="325"/>
        <source>Toolbar button style:</source>
        <translation>Estil del botó de la barra d&apos;eines:</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="333"/>
        <source>Only display the icon</source>
        <translation>Mostra únicament la icona</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="338"/>
        <source>Only display the text</source>
        <translation>Mostra únicament el text</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="343"/>
        <source>The text appears beside the icon</source>
        <translation>El text apareix al costat de la icona</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="348"/>
        <source>The text appears under the icon</source>
        <translation>El text apareix a sota de la icona</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="353"/>
        <source>Default</source>
        <translation>Predeterminat</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="361"/>
        <source>Activate item on single click</source>
        <translation>Activa l&apos;ítem amb un sol clic</translation>
    </message>
    <message>
        <location filename="../styleconfig.ui" line="51"/>
        <source>Qt Style</source>
        <translation>Estil Qt</translation>
    </message>
    <message>
        <location filename="../styleconfig.cpp" line="294"/>
        <location filename="../styleconfig.cpp" line="304"/>
        <source>Save Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.cpp" line="294"/>
        <source>Palette name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.cpp" line="305"/>
        <source>A palette with the same name exists.
Do you want to replace it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.cpp" line="374"/>
        <source>Remove Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../styleconfig.cpp" line="375"/>
        <source>Do you really want to remove selected palette(s)?
Root palettes will remain intact if existing.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
